PRES1 = $(wildcard Seance1/Presentation/*.tex)
PRES2 = $(wildcard Seance2/Presentation/*.tex)

all : seance1 seance2 clean

tp1 : Seance1/TP/tp.tex
	cd Seance1/TP && make

presentation1 : $(PRES1)
	cd Seance1/Presentation && make

seance1 : tp1 presentation1

tp2 : Seance2/TP/tp.tex
	cd Seance2/TP && make

presentation2 : $(PRES2)
	cd Seance2/Presentation && make

seance2 : tp2 presentation2

clean :
	cd Seance2/TP && make clean
	cd Seance2/Presentation && make clean
	cd Seance1/TP && make clean
	cd Seance1/Presentation && make clean

rmpdf :
	cd Seance2/TP && make rmpdf
	cd Seance2/Presentation && make rmpdf
	cd Seance1/TP && make rmpdf
	cd Seance1/Presentation && make rmpdf